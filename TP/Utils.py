class Item:
    index = 0
    title = "Objeto vazio"
    obj = None

    def __init__(self, title=None, obj=None):
        self.index = 0
        self.title = title or 'Objeto vazio'
        self.obj = obj or None

    def __repr__(self):
        idx = "'index': {}".format(self.index)
        title = "'title': {}".format(self.title)
        obj = "'obj': {}".format(self.obj)
        return "{" + idx + ", " + title + ", " + obj + "}"

    def __str__(self):
        idx = "'index': {}".format(self.index)
        title = "'title': {}".format(self.title)
        obj = "'obj': {}".format(self.obj)
        return "{" + idx + ", " + title + ", " + obj + "}"

    def __getitem__(self, item):
        if item == "index":
            return self.index
        elif item == "title":
            return self.title
        elif item == "obj":
            return self.obj
        else:
            return None

    def __getattr__(self, item):
        return item.upper()

    def print_obj(self):
        return self.title


class ItemsList:
    last_item_index = 0
    items = []

    def __getattr__(self, item):
        return item.upper()

    def __getitem__(self, item):
        if item == "last_item_index":
            return self.last_item_index
        elif item == "items":
            return self.items
        else:
            return None

    def __repr__(self):
        return {'items': self.items, 'last_item_index': self.last_item_index}

    def __str__(self):
        items = "'items': {}".format(self.items)
        last_item_index = "'last_item_index': {}".format(self.last_item_index)
        return "{" + items + ", " + last_item_index + "}"

    def __init__(self, items_list=[]):
        self.last_item_index = 0
        self.items = []
        if not isinstance(items_list, list):
            items_list = [items_list]
        for item in items_list:
            if str(type(item)) != "<class 'Utils.Item'>":
                return None
            else:
                item.index = self.last_item_index
                self.last_item_index += 1
                self.items.append(item)

    def insert(self, items_list=[]):
        if not isinstance(items_list, list):
            items_list = [items_list]
        for item in items_list:
            if str(type(item)) != "<class 'Utils.Item'>":
                return None
            else:
                item.index = self.last_item_index
                self.last_item_index += 1
                self.items.append(item)

    def remove(self, item=None):
        if item is not None:
            if not isinstance(item, Item):
                to_remove = self.get_item_by_title(item)
                self.items.remove(to_remove)
            else:
                self.items.remove(item)

    def get_items(self):
        return [x for x in self.items]

    def get_item_by_title(self, title=None):
        if title is not None:
            for item in self.items:
                if item.title == title:
                    return item


class Circ:

    center = 0
    raio = 0
    pixels = []

    def __init__(self, pixels=None, center=None, raio=None):
        self.center = None
        self.raio = None
        self.pixels = []
        if None not in (pixels, center, raio):
            self.pixels = pixels
            self.center = center
            self.raio = raio

    def __getattr__(self, item):
        return item.upper()

    def __getitem__(self, item):
        if item == 'center':
            return self.center
        if item == 'raio':
            return self.raio
        if item == 'pixels':
            return self.pixels
        return None

    def __repr__(self):
        return {'center': self.center, 'raio': self.raio, 'pixels': self.pixels}

    def __str__(self):
        center = "'center': {}".format(self.center)
        raio = "'raio': {}".format(self.raio)
        pixels = "'pixels': {}".format(self.pixels)
        return "{" + center + ", " + raio + ", " + pixels + "}"


class Line:
    center = None
    vert = None
    pixels = []

    def __init__(self, pixels=None, center=None):
        self.center = None
        self.ver = None
        self.pixels = []
        if None not in (pixels, center):
            self.center = center
            self.pixels = pixels
            self.vert = (pixels[0], pixels[-1])

    def __getitem__(self, item):
        if item == 'center':
            return self.center
        if item == 'vert':
            return self.vert
        if item == 'pixels':
            return self.pixels
        return None

    def __getattr__(self, item):
        return item.upper()

    def __repr__(self):
        return {'center': self.center, 'vert': self.vert, 'reta': self.pixels}

    def __str__(self):
        center = "'center': {}".format(self.center)
        vert = "'vert': {}".format(self.vert)
        pixels = "'pixels': {}".format(self.pixels)
        return "{" + center + ", " + vert + ", " + pixels + "}"

    def __del__(self):
        print("Line destroy.")
