from tkinter import *
from Utils import *
import math


class PaintApp:

    left_button = "up"
    right_button = "up"

    x_pos, y_pos = None, None
    x1, y1, x2, y2 = None, None, None, None

    line = None
    alg_select = None
    do_alg = None
    status_default = "Nenhum algoritmo selecionado."
    status = ""

    all_obj = ItemsList()

# -------- Mouse Events --------

    # ---- Motion ----
    def motion(self, event=None):
        if self.left_button == "down" and self.alg_select:
            if None not in (self.x1, self.y1, event.x, event.y):
                if self.line:
                    self.drawing_area.delete(self.line)
                self.line = self.drawing_area.create_line(self.x1, self.y1, event.x, event.y, smooth=TRUE, fill="black")
        elif self.left_button == "down":
            if None not in (self.x1, self.y1, event.x, event.y):
                if self.line:
                    self.drawing_area.delete(self.line)
                self.line = self.drawing_area.create_rectangle(self.x1, self.y1, event.x, event.y, fill="")

    # ---- Left button ----
    def left_bt_down(self, event=None):
        self.left_button = "down"

        self.x1 = event.x
        self.y1 = event.y

    def left_bt_up(self, event=None):
        self.left_button = "up"
        resp = None

        if self.alg_select == "line_dda":
            if None not in (self.x1, self.y1, event.x, event.y):
                parameters = (self.x1, self.y1, event.x, event.y)

        elif self.alg_select == "line_bresenham":
            if None not in (self.x1, self.y1, event.x, event.y):
                parameters = (self.x1, self.y1, event.x, event.y)

        elif self.alg_select == "circ_bresenham":
            if None not in (self.x1, self.y1, event.x, event.y):
                parameters = (self.x1, self.y1, event.x, event.y)

        else:
            if None not in (self.x1, self.y1, event.x, event.y):
                parameters = (self.x1, self.y1, event.x, event.y)

        if self.line:
            self.drawing_area.delete(self.line)
        try:
            resp = self.do_alg(parameters)
        except Exception as e:
            print(e)

        if not resp:
            print("Algo deu errado.")

        self.x1 = None
        self.y1 = None

    # ---- Right button ----
    def right_bt_down(self, event=None):
        self.reset_coords()
        self.reset_options()
        self.reset_selection()

# -------- Line Algorithm --------

    def line_dda(self, parameters=None):
        if None in parameters:
            return False
        x1, y1, x2, y2 = map(int, parameters)

        dda = Item()
        linha = Line()
        x, y, dx, dy = x1, y1, x2-x1, y2-y1

        if abs(dx) >= abs(dy):
            passos = abs(dx)
        else:
            passos = abs(dy)

        xincr = dx/passos
        yincr = dy/passos

        for i in range(passos):
            x += xincr
            y += yincr
            pixel = self.draw_pixel(x, y)
            linha.pixels.append(pixel)

        linha.vert = (linha.pixels[0], linha.pixels[-1])
        linha.center = linha.pixels[int(len(linha.pixels)/2)+1]
        dda.index = self.all_obj.last_item_index
        dda.title = "LINE DDA - "+str(dda.index)
        dda.obj = linha

        self.all_obj.insert(dda)

        del linha

        return True

    def line_bresenham(self, parameters=None):
        if None in parameters:
            return False

        x1, y1, x2, y2 = map(int, parameters)
        bresenham = Item()
        linha = Line()
        x, y, dx, dy = x1, y1, x2-x1, y2-y1

        if dx > 0:
            xincr = 1
        else:
            xincr = -1
            dx = -dx
        if dy > 0:
            yincr = 1
        else:
            yincr = -1
            dy = -dy


        pixel = self.draw_pixel(x, y)
        linha.pixels.append(pixel)
        if dx > dy:
            p = 2*dy-dx
            c1 = 2*dy
            c2 = 2*(dy-dx)
            for i in range(dx):
                x += xincr
                if p < 0:
                    p += c1
                else:
                    y += yincr
                    p += c2
                pixel = self.draw_pixel(x, y)
                linha.pixels.append(pixel)
        else:
            p = 2*dx-dy
            c1 = 2*dx
            c2 = 2*(dx-dy)
            for i in range(dy):
                y += yincr
                if p < 0:
                    p += c1
                else:
                    x += xincr
                    p += c2
                pixel = self.draw_pixel(x, y)
                linha.pixels.append(pixel)


        linha.vert = (linha.pixels[0], linha.pixels[-1])
        linha.center = linha.pixels[int(len(linha.pixels)/2)+1]
        bresenham.index = self.all_obj.last_item_index
        bresenham.title = "LINE BRESENHAM - " + str(bresenham.index)
        bresenham.obj = linha

        self.all_obj.insert(bresenham)

        return True

# -------- Circ Algorithm --------

    def circ_bresenham(self, parameters=None):
        if None in parameters:
            return False
        bresenham = Item()
        circ = Circ()
        xc, yc, x2, y2 = map(int, parameters)
        dx = abs(x2 - xc)
        dy = abs(y2 - yc)
        r = round(math.sqrt(math.pow(dx, 2) + math.pow(dy, 2)))
        x = 0
        y = r
        p = 3 - 2 * r
        pixels = self.plot_simetrics(x, y, xc, yc)
        for pixel in pixels:
            circ.pixels.append(pixel)
        while x < y:
            if p < 0:
                p += 4*x + 6
            else:
                p += 4*(x-y) + 10
                y -= 1
            x += 1
            pixels = self.plot_simetrics(x, y, xc, yc)
            for pixel in pixels:
                circ.pixels.append(pixel)

        circ.center = (xc, yc)
        circ.raio = r
        bresenham.index = self.all_obj.last_item_index
        bresenham.title = "CIRC BRESENHAM - "+str(bresenham.index)
        bresenham.obj = circ

        self.all_obj.insert(bresenham)

        return True

    def plot_simetrics(self, x, y, xc, yc):
        pixels = []
        if None not in (x, y, xc, yc):
            pixel = self.draw_pixel(xc + x, yc + y)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc + x, yc - y)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc - x, yc + y)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc - x, yc - y)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc + y, yc + x)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc + y, yc - x)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc - y, yc + x)
            pixels.append(pixel)
            pixel = self.draw_pixel(xc - y, yc - x)
            pixels.append(pixel)
        return pixels

# -------- Draw commands --------

    def draw_pixel(self, x=None, y=None):
        if None in (x,y):
            return []
        return self.drawing_area.create_rectangle(x, y, x, y, fill="black")

    def selection(self, parameters=None):
        x1, y1, x2, y2 = map(int, parameters)

        enclosed = self.drawing_area.find_enclosed(x1, y1, x2, y2)
        items_enclosed = []

        self.list_selection_listbox.delete(0, END)
        for item in self.all_obj.items:
            pixels_in = []
            for pixel in item.obj.pixels:
                if pixel in enclosed:
                    pixels_in.append(pixel)

            ratio = float(len(pixels_in)/len(item.obj.pixels))
            if ratio >= 0.75:
                items_enclosed.append(item)
                self.list_selection_listbox.insert(END, item.title)

        return True

# -------- Algorithm selection --------

    def select_line_dda(self):
        self.alg_select = "line_dda"
        self.update_status_bar("Algoritmo 'Linha DDA' selecionado.")
        self.do_alg = self.line_dda

    def select_line_bresenham(self):
        self.alg_select = "line_dda"
        self.status.set("Algoritmo 'Linha Bresenham' selecionado.")
        self.do_alg = self.line_bresenham

    def select_circ_bresenham(self):
        self.alg_select = "circ_bresenham"
        self.status.set("Algoritmo 'Circunferência Bresenham' selecionado.")
        self.do_alg = self.circ_bresenham

# -------- Update/Reset --------

    def reset_coords(self):
        self.x1, self.x2, self.x_pos = None, None, None
        self.y1, self.y2, self.y_pos = None, None, None

    def reset_options(self):
        self.do_alg = self.selection
        self.alg_select = None
        self.line = None
        self.update_status_bar()

    def remove_obj(self, obj=None):
        if obj:
            for pixel in obj.obj.pixels:
                self.drawing_area.delete(pixel)

    def reset_selection(self):
        self.list_selection_listbox.delete(0, END)

    def update_status_bar(self, text=status_default):
        self.status.set(text)

    def delete_single_selection(self):
        selected_item_title = self.list_selection_listbox.get(ANCHOR)
        if selected_item_title:
            selected_item = self.all_obj.get_item_by_title(selected_item_title)
            idx = self.list_selection_listbox.get(0, END).index(selected_item_title)
            self.list_selection_listbox.delete(idx)
            self.all_obj.remove(selected_item)
            self.remove_obj(selected_item)

    def delete_all(self):
        for item in self.list_selection_listbox.get(0, END):
            selected_item = self.all_obj.get_item_by_title(item)
            self.list_selection_listbox.delete(0)
            self.all_obj.remove(selected_item)
            self.remove_obj(selected_item)

# -------- Create Canvas --------

    def __init__(self, master=None):
        self.root = master

        self.options_menu = Frame(self.root, padx=5, pady=5)
        self.options_menu.pack(side=LEFT)

        self.lines = Frame(self.options_menu)
        Label(self.lines, text="LINHAS:", font=("Helvetica", 10), pady=10, fg="black").pack()
        self.line_dda_bt = Button(self.lines, text="DDA", font=("Helvetica", 10), pady=5, command=self.select_line_dda)
        self.line_dda_bt.pack(fill="y")
        self.line_bresenham_bt = Button(self.lines, text="Bresenham", font=("Helvetica", 10), pady=5, command=self.select_line_bresenham)
        self.line_bresenham_bt.pack(fill="y")
        self.lines.pack()

        self.circ = Frame(self.options_menu)
        Label(self.circ, text="CIRCUNFERÊNCIAS:", font=("Helvetica", 10), pady=10, fg="black").pack()
        self.circ_bresenham_bt = Button(self.circ, text="Bresenham", font=("Helvetica", 10), pady=5, command=self.select_circ_bresenham)
        self.circ_bresenham_bt.pack(fill="y")
        self.circ.pack()

        self.list_menu = Frame(self.root, padx=5, pady=5)
        self.list_menu.pack(side=RIGHT)

        self.selection_list = Frame(self.list_menu)
        Label(self.selection_list, text="Selecionado(s):", font=("Helvetica", 10), pady=10, fg="black").pack()
        self.list_selection_listbox = Listbox(self.selection_list, width=20, height=15, font=("Helvetica", 10))
        self.list_selection_listbox.pack(side=LEFT, fill="y")
        self.list_selection_scrollbar = Scrollbar(self.selection_list, orient="vertical")
        self.list_selection_scrollbar.config(command=self.list_selection_listbox.yview)
        self.list_selection_scrollbar.pack(side=RIGHT, fill="y")
        self.list_selection_listbox.config(yscrollcommand=self.list_selection_scrollbar.set)
        self.selection_list.pack()

        self.delete_frame = Frame(self.list_menu, padx=5)
        self.list_selection_delete = Button(self.delete_frame, text="Deletar", font=("Helvetica", 10), pady=5,
                                            command=self.delete_single_selection)
        self.list_selection_delete.pack(side=LEFT)
        self.list_selection_delete_all = Button(self.delete_frame, text="Deletar tudo", font=("Helvetica", 10), pady=5,
                                                command=self.delete_all)
        self.list_selection_delete_all.pack(side=RIGHT)
        self.delete_frame.pack()

        self.status = StringVar()
        self.update_status_bar()
        self.status_bar = Label(self.root, textvariable=self.status, font=("Helvetica", 10), pady=5, fg="black")
        self.status_bar.pack(side=TOP)

        self.drawing_area = Canvas(self.root, width=600, height=500, bg="#ffffff")
        self.drawing_area.pack(fill="both")

        self.drawing_area.bind("<Motion>", self.motion)
        self.drawing_area.bind("<ButtonPress-1>", self.left_bt_down)
        self.drawing_area.bind("<ButtonRelease-1>", self.left_bt_up)
        self.drawing_area.bind("<ButtonPress-3>", self.right_bt_down)

        self.do_alg = self.selection


master = Tk()
app = PaintApp(master)
master.mainloop()
