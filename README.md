# TP - Unidade 1

Primeiramente, clone o conteúdo desse repositório.

```

git clone https://dkuma25@bitbucket.org/dkuma25/tp-unidade-1.git
```

## Instalação

Para instalar o programa, entre na Pasta TP e execute o arquivo *build.bat*.

## Execução

Para executar o programa, entre na pasta criada na instalação e siga o fluxo:

*build* > *exe.win-amd64-3.7* > e execute o arquivo *TP.exe*



# O Programa

Esse programa possui ferramentas para criar e deletar retas e circunferências com os
algoritmos DDA e Bresenham, além de poder selecionar os objetos criados.
